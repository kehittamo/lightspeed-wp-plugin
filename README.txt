=== Plugin Name ===
 * @link              http://www.kehittamo.fi
 * @since             1.0.0
 * @package           Lightspeed_Wp_Plugin
 *
 * @wordpress-plugin
 * Plugin Name:       Lightspeed WP plugin
 * Plugin URI:        http://www.kehittamo.fi
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Kehittämö Oy / Jani Krunniniva
 * Author URI:        http://www.kehittamo.fi
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       lightspeed-wp-plugin
 * Domain Path:       /languages