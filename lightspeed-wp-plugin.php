<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://www.kehittamo.fi
 * @since             1.0.0
 * @package           Lightspeed_Wp_Plugin
 *
 * @wordpress-plugin
 * Plugin Name:       Lightspeed WP plugin
 * Plugin URI:        http://www.kehittamo.fi
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Kehittämö Oy / Jani Krunniniva
 * Author URI:        http://www.kehittamo.fi
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       lightspeed-wp-plugin
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-lightspeed-wp-plugin-activator.php
 */
function activate_lightspeed_wp_plugin() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-lightspeed-wp-plugin-activator.php';
	Lightspeed_Wp_Plugin_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-lightspeed-wp-plugin-deactivator.php
 */
function deactivate_lightspeed_wp_plugin() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-lightspeed-wp-plugin-deactivator.php';
	Lightspeed_Wp_Plugin_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_lightspeed_wp_plugin' );
register_deactivation_hook( __FILE__, 'deactivate_lightspeed_wp_plugin' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-lightspeed-wp-plugin.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_lightspeed_wp_plugin() {
	$plugin_messages = array();

	//Plugin check
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

	if ( ! is_plugin_active( 'rest-api-oauth1/oauth-server.php' ) ) {
		$plugin_messages[] = '<a href="https://wordpress.org/plugins/rest-api-oauth1/" target="_blank">WP REST API - OAuth 1.0a Server -plugin.</a> ("wpackagist-plugin/rest-api-oauth1": "^0.3.0")';
	}

	if ( ! is_plugin_active( 'wp-api-menus/wp-api-menus.php' ) ) {
		$plugin_messages[] = '<a href="https://wordpress.org/plugins/wp-api-menus/" target="_blank">WP REST API - Menus -plugin.</a> ("wpackagist-plugin/wp-api-menus" : "*")';
	}


	//Now that we have our messages, let show it to our users
	if ( count( $plugin_messages ) > 0 ) {
		// TODO: Exiting doesnt work correctly: gives error: Plugin could not be activated because it triggered a fatal error.
		new lightspeed_admin_Message( implode('<br/>' , $plugin_messages) );
	} else {
		$plugin = new Lightspeed_Wp_Plugin();
		$plugin->run();
	}
}
run_lightspeed_wp_plugin();

class lightspeed_admin_Message {
	private $_message;

	function __construct( $message ) {
		$this->_message = $message;
		add_action( 'admin_notices', array( $this, 'render' ) );
	}

	function render() {
		printf( '<div class="error"><h2>Lightspeed WP plugin requires:</h2><p>%s</p></div>', $this->_message );
	}
}
