<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.kehittamo.fi
 * @since      1.0.0
 *
 * @package    Lightspeed_Wp_Plugin
 * @subpackage Lightspeed_Wp_Plugin/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Lightspeed_Wp_Plugin
 * @subpackage Lightspeed_Wp_Plugin/includes
 * @author     Kehittämö Oy / Jani Krunniniva <asiakaspalvelu@kehittamo.fi>
 */
class Lightspeed_Wp_Plugin_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
