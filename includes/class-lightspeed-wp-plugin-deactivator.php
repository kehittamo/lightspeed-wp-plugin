<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://www.kehittamo.fi
 * @since      1.0.0
 *
 * @package    Lightspeed_Wp_Plugin
 * @subpackage Lightspeed_Wp_Plugin/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Lightspeed_Wp_Plugin
 * @subpackage Lightspeed_Wp_Plugin/includes
 * @author     Kehittämö Oy / Jani Krunniniva <asiakaspalvelu@kehittamo.fi>
 */
class Lightspeed_Wp_Plugin_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
