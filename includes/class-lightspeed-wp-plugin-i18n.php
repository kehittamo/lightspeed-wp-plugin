<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://www.kehittamo.fi
 * @since      1.0.0
 *
 * @package    Lightspeed_Wp_Plugin
 * @subpackage Lightspeed_Wp_Plugin/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Lightspeed_Wp_Plugin
 * @subpackage Lightspeed_Wp_Plugin/includes
 * @author     Kehittämö Oy / Jani Krunniniva <asiakaspalvelu@kehittamo.fi>
 */
class Lightspeed_Wp_Plugin_i18n {

	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'lightspeed-wp-plugin',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);
	}
}
