<?php

/**
 * WP-API routes for custom frontpage data
 * @see: http://v2.wp-api.org/extending/adding/
 */
class Lightspeed_REST_API_Endpoints {

	public function __construct() {
		$this->namespace     = 'widgets-data/v1';
		$this->namespace_dev = 'dev-widgets-data/v1';
		$this->resource_name = 'posts';

		$this->namespace_lightspeed = 'lightspeed-data/v1';
	}

	/**
	 * Register the routes for the objects of the controller.
	 */
	public function register_routes() {

		/** LIGHTSPEED ROUTES : GET: get ============================= */
		register_rest_route( $this->namespace_lightspeed, '/get',
			array(
				'methods'  => WP_REST_Server::READABLE,
				'callback' => [ $this, 'get_lightspeed_data' ]
			)
		);

		/** FRONTPAGE ROUTES : GET: get ============================= */
		register_rest_route( $this->namespace, '/get',
			array(
				'methods'  => WP_REST_Server::READABLE,
				'callback' => [ $this, 'get_widgets_data' ]
			)
		);

		/** FRONTPAGE ROUTES : POST: add ============================= */
		register_rest_route( $this->namespace, '/add', array(
			'methods'             => WP_REST_Server::CREATABLE,
			'callback'            => function ( WP_REST_Request $req ) {
				// TODO: move callback to own function
				$params         = $req->get_json_params();
				$widgets_data = array(
					'widgets_data'                => $params['widgets_data'],
					'latest_update_by_user_id' => get_current_user_id(),
					'update_time'              => time(),
				);
				$updated = update_option( '_custom_client_widgets_data', $widgets_data, false );
				wp_cache_delete( '_custom_client_widgets_data', 'options' );

				return array(
					'result'  => $params['widgets_data'],
					'updated' => $updated,
				);
			},
			'args'                => array(
				'widgets_data' => array(
					'required'          => true,
					'validate_callback' => function ( $param, $request, $key ) {
						// TODO: some validation?
						return true;
					},
				),
			),
			'permission_callback' => function () {
				// TODO: also editors can update?
				// TODO: move to own function if permission check is needed to somewhere else
				return current_user_can( 'administrator' );
			}
		) );

		/** FRONTPAGE ROUTES : GET: get ============================= */
		register_rest_route( $this->namespace_dev, '/get',
			array(
				'methods'  => WP_REST_Server::READABLE,
				'callback' => [ $this, 'get_dev_widgets_data' ]
			)
		);

		/** FRONTPAGE ROUTES : POST: add ============================= */
		register_rest_route( $this->namespace_dev, '/add', array(
			'methods'             => WP_REST_Server::CREATABLE,
			'callback'            => function ( WP_REST_Request $req ) {
				// TODO: move callback to own function
				$params         = $req->get_json_params();
				$widgets_data = array(
					'widgets_data'                => $params['widgets_data'],
					'latest_update_by_user_id' => get_current_user_id(),
					'update_time'              => time(),
				);
				$updated = update_option( '_dev_custom_client_widgets_data', $widgets_data, true );
				wp_cache_delete( '_dev_custom_client_widgets_data', 'options' );

				return array(
					'result'  => $params['widgets_data'],
					'updated' => $updated,
				);
			},
			'args'                => array(
				'widgets_data' => array(
					'required'          => true,
					'validate_callback' => function ( $param, $request, $key ) {
						// TODO: some validation?
						return true;
					},
				),
			),
			'permission_callback' => function () {
				// TODO: also editors can update?
				// TODO: move to own function if permission check is needed to somewhere else
				return current_user_can( 'administrator' );
			}
		) );
	}

	/**
	 * Get Frontpage data
	 *
	 * @param WP_REST_Request $request Full data about the request.
	 *
	 * @return WP_Error|WP_REST_Request|WP_REST_Response
	 */
	public function get_lightspeed_data( \WP_REST_Request $request ) {
		$data = array(
			'home_url' => get_option( 'home' ),
			'routes'   => array(
				'categories' => get_option( 'category_base' ),
				'posts'      => get_option( 'permalink_structure' ),
				'tags'       => get_option( 'tag_base' )
			),
			'menus'    => get_terms( 'nav_menu' )
		);

		return new WP_REST_Response( $data, 200 );
	}

	/**
	 * Get Frontpage data
	 *
	 * @param WP_REST_Request $request Full data about the request.
	 *
	 * @return WP_Error|WP_REST_Request|WP_REST_Response
	 */
	public function get_widgets_data( \WP_REST_Request $request ) {
		$widgets_data = get_option( '_custom_client_widgets_data' );
		$data           = array(
			'result' => $widgets_data
		);

		return new WP_REST_Response( $data, 200 );
	}

	/**
	 * Get Frontpage data
	 *
	 * @param WP_REST_Request $request Full data about the request.
	 *
	 * @return WP_Error|WP_REST_Request|WP_REST_Response
	 */
	public function get_dev_widgets_data( \WP_REST_Request $request ) {
		$widgets_data = get_option( '_dev_custom_client_widgets_data' );
		$data           = array(
			'result' => $widgets_data
		);

		return new WP_REST_Response( $data, 200 );
	}


	/**
	 * Return only certain fields if set
	 *
	 * @param array $data    Data.
	 * @param $post   Post.
	 * @param $context Context.
	 */
	function rest_api_filter_fields_post( $data, $post, $context ) {
		if ( ! empty( $_GET['fields'] ) ) {
			$new_data = array();
			$fields = explode( ',', $_GET['fields'] );
			if ( empty( $fields ) || count( $fields ) == 0 ) {
				return $data;
			}
			foreach ( $data->data as $key => $value ) {
				if ( in_array( $key, $fields ) ) {
					$new_data[ $key ] = $value;
				}
			}
			if ( 0 < sizeof( $new_data ) ) {
				$data->data = $new_data;
			}
		}
		return $data;
	}
}
